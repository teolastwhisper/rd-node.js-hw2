const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(3)
        .max(20)
        .required(),
    password: Joi.string()
        .min(3)
        .max(20)
        .required(),

  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};
const addNoteValidator = async (req, res, next) => {
  console.log( await req.body);
  const schema = Joi.object({
    text: Joi.string()
        .max(128)
        .required(),

  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};
module.exports = {
  registrationValidator,
  addNoteValidator,
};
