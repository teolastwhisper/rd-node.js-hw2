const express = require('express');
const router = express.Router();
const {
  getUserById,
  deleteUserById,
  changeUserPass,
} = require('../services/usersService');
const {
  asyncWrapper,
} = require('../utils/apiUtils');
router.get('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserById(userId);

  if (!user) {
    throw new InvalidRequestError('No user with such id found!');
  }

  res.json({user});
}));
router.delete('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserById(userId);

  res.json({message: 'User deleted successfully'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {newPassword, oldPassword} = req.body;

  await changeUserPass(userId, newPassword, oldPassword);

  res.json({message: 'User\'s password updated successfully'});
}));
module.exports = {
  usersRouter: router,
};
