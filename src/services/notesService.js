const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId, notePayload) => {
  const {limit, offset} = notePayload;
  const notes = await Note.find({userId})
      .limit(parseInt(limit, 10))
      .skip(parseInt(offset, 10));
  return notes;
};

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  return note;
};

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
};

const updateNoteByIdForUser = async (noteId, userId, text) => {
  await Note.findOneAndUpdate(
      {_id: noteId, userId},
      {$set: {text: text}},
  );
};
const patchNoteByIdForUser = async (noteId, userId, completed) => {
  await Note.findOneAndUpdate(
      {_id: noteId, userId},
      {$set: {completed: completed}},
  );
};
const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

module.exports = {
  getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  patchNoteByIdForUser,
  deleteNoteByIdForUser,
};
